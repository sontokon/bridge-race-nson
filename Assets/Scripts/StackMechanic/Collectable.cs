using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class Collectable : MonoBehaviour
{
    public Color color;
    public bool isOnStack = false;

    private IObjectPool<Collectable> _pool;

    public void SetPool(IObjectPool<Collectable> pool) => _pool = pool;

    public void Release()
    {
        _pool?.Release(this);
        gameObject.GetComponent<Collider>().enabled = true;
    }    

    private void OnTriggerEnter(Collider other)
    {
        CollectBrick(other);
    }
    void CollectBrick(Collider other)
    {
        if(other.transform.GetComponent<PlayerScript>().playerColor ==  color)
        {
            other.transform.GetComponent<StackManager>().PushBrick(gameObject);
            gameObject.GetComponent<Collider>().enabled = false;
            isOnStack = true;
        }
    }    
}
