using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class StackManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> bricks = new List<GameObject>();

    [SerializeField] private GameObject stackPoint;
    [SerializeField] private float stackYIncreaseRate = 1f;
    [SerializeField] private float stackXPosition = 0.18f;
    //[SerializeField] private Collectable prefabBrick;
    //private ObjectPool<Collectable> poolBrickGreen;

    public void PushBrick(GameObject collectedBrick)
    {
        bricks.Add(collectedBrick);
        MoveToStackAnim(collectedBrick);
    }
    void MoveToStackAnim(GameObject brick)
    {
        Vector3 targetPosition;

        if (bricks.Count == 1)
        {
            targetPosition = new Vector3(stackXPosition, stackYIncreaseRate, 0);
            print("first brick");
        }
        else
        {   
            targetPosition = new Vector3(stackXPosition, bricks.Count  * stackYIncreaseRate, 0);
            print("not first brick");
        }

        brick.transform.parent = stackPoint.transform;
        brick.transform.DOLocalMove(targetPosition, 0.2f);
        brick.transform.rotation = stackPoint.transform.rotation;
    }

    public bool IsPopAble()
    {
        if(bricks.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }    
    }   
    public void PopBrick()
    {
        if (bricks.Count > 0)
        {
            GameObject brickToRemove = bricks[bricks.Count - 1];
            bricks.RemoveAt(bricks.Count - 1);

            brickToRemove.GetComponent<Collectable>().Release();
            //Destroy(brickToRemove);
        }
        else
        {
            Debug.Log("No bricks to pop!");
        }
    }    


}
