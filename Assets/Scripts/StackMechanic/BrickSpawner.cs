using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Pool;

public class BrickSpawner : MonoBehaviour
{
    [SerializeField] private int numOfItemsToSpawn;
    //private int numOfPlayers = 4;

    [SerializeField] GameObject brickSpawnPoint;

    private GameObject brickSpawnArea;

    [SerializeField] bool isStartArea;
    [SerializeField] LayerMask layerMask;

    //private ObjectPool<Collectable> poolBrick ;
    [SerializeField] List<Collectable> collectablePrefabs;
    private List<ObjectPool<Collectable>> poolCollectables = new();

    private void Awake()
    {
        InitPool();
    }

    private void Start()
    {
        //InitPool();
        //poolBrick = new ObjectPool<Collectable>(CreateBrick, OnGetBrick, OnRealseBrick);
        //CreateCollectablePool();
        brickSpawnArea = gameObject.transform.GetChild(0).gameObject;
        if (isStartArea)
        {
            SpawnItemsAtStart(numOfItemsToSpawn);
        }
    }


    #region Pool

    private void InitPool()
    {
        for (var index = 0; index < collectablePrefabs.Count; index++)
        {
            var brick = collectablePrefabs[index];
            var copyIndex = index;
            poolCollectables.Add(new ObjectPool<Collectable>(() =>
            {
                var newObject = Instantiate(brick);
                newObject.SetPool(poolCollectables[copyIndex]);
                return newObject;
            }, ant1 =>
            {
                ant1.gameObject.SetActive(true);
                //poolCollectables.Add(ant1);
            }, ant1 =>
            {
                ant1.gameObject.SetActive(false);
                //ant1.transform.position = randomizeSpawnPoint();
                //ant1.transform.rotation = Quaternion.Euler(0, 0, 0);
                //ant1.transform.parent = brickSpawnPoint.transform;
                //var brickobj1 = GetBrick();
                //brickobj1.transform.parent = brickSpawnPoint.transform;

            }));
        }
    }

    public Collectable GetRedBrick()
    {
        return poolCollectables[0].Get();
    }

    public Collectable GetGreenBrick()
    {
        return poolCollectables[1].Get();
    }

    public Collectable GetBlueBrick()
    {
        return poolCollectables[2].Get();
    }
    public Collectable GetYellowBrick()
    {
        return poolCollectables[3].Get();
    }





    //private void OnRealseBrick(Collectable collectable)
    //{
    //    collectable.gameObject.SetActive(false);
    //    collectable.transform.position = randomizeSpawnPoint();
    //    collectable.transform.rotation = Quaternion.Euler(0, 0, 0);
    //    collectable.transform.parent = brickSpawnPoint.transform;
    //    var brickobj1 = GetBrick();
    //    brickobj1.transform.parent = brickSpawnPoint.transform;

    //}

    //private void OnGetBrick(Collectable collectable)
    //{
    //    collectable.gameObject.SetActive(true);
    //}
    //private Collectable CreateBrick()
    //{
    //    Vector3 targetPos = randomizeSpawnPoint();
    //    Collider[] colliders = Physics.OverlapSphere(targetPos, 1f, layerMask);
    //    while (colliders.Length != 0)
    //    {
    //        targetPos = randomizeSpawnPoint();
    //        colliders = Physics.OverlapSphere(targetPos, 1f, layerMask);
    //    }
    //    var brick = Instantiate(collectablePrefabs[0].GetComponent<Collectable>(),targetPos, Quaternion.Euler(0, 0, 0));
    //    brick.SetPool(poolBrick);
    //    return brick;
    //}
    //public Collectable GetBrick()
    //{
    //    return poolBrick.Get();
    //}
    #endregion
    private void SpawnItemsAtStart(int numItemsToSpawn)
    {
        for (int i = 0; i < collectablePrefabs.Count; i++)
        {
            for (int j = 0; j < numItemsToSpawn; j++)
            {
                Vector3 targetPos = randomizeSpawnPoint();
                Collider[] colliders = Physics.OverlapSphere(targetPos, 1f, layerMask);
                while (colliders.Length != 0)
                {
                    targetPos = randomizeSpawnPoint();
                    colliders = Physics.OverlapSphere(targetPos, 1f, layerMask);
                }

                print(i);
                var brick = poolCollectables[i].Get();
                brick.transform.position = targetPos;
                brick.transform.rotation = Quaternion.identity;
                brick.transform.parent = brickSpawnPoint.transform;

                //var brickTest = Instantiate(collectablePrefabs[1].GetComponent<Collectable>(), targetPos, Quaternion.Euler(0, 0, 0));
                //var brickobj = GetBrick();
                //var brickobj = GetRandomPool();
                //brickobj.transform.parent = brickSpawnPoint.transform;
            }
        }


    }

    //public static event Action OnPopItem;
    //public void ReSpawnItemOnPop()
    //{
    //    OnPopItem?.Invoke();
    //    var brickobj1 = GetBrick();
    //    brickobj1.transform.parent = brickSpawnPoint.transform;
    //    print("run this func");
    //}

    Vector3 randomizeSpawnPoint()
    {
        // Calculate Bounds
        var meshRenderer = brickSpawnArea.GetComponent<MeshRenderer>();
        Bounds meshBounds = meshRenderer.bounds;

        //Debug.Log(meshBounds.min.x + " " + meshBounds.max.x + " " + meshBounds.min.z + " " + meshBounds.max.z);

        return new Vector3(UnityEngine.Random.Range(meshBounds.min.x, meshBounds.max.x), meshBounds.max.y + 0.164f, UnityEngine.Random.Range(meshBounds.min.z, meshBounds.max.z));
    }
}
