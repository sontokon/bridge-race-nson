using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private FloatingJoystick joystick;
    [SerializeField] private CharacterController controller;
    [SerializeField] private float moveSpeed;


    private void FixedUpdate()
    {
        HandleJoystick();
    }
    public void HandleJoystick()
    {
        float horizontal = joystick.Horizontal;
        float vertical = joystick.Vertical;
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;
        controller.SimpleMove(direction * moveSpeed * Time.deltaTime);
        if (direction.magnitude >= 0.1f)
        {
            animator.SetInteger("AnimState", 1);
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);
        }
        else
        {
            animator.SetInteger("AnimState", 0);
        }
    }

}
