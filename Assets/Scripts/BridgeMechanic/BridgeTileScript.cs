using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeTileScript : MonoBehaviour
{
    public int colorIndex;
    [SerializeField] Material brickMaterials;
    [SerializeField] private Renderer brickRenderer;

    public bool hasBrick = false;

    Color targetColor;

    public void ColorBrick(int _targetColorIndex)
    {
        if (!hasBrick)
        {
        hasBrick = true;
        brickRenderer.enabled = true;
        brickRenderer.material.SetColor("_Color", brickMaterials.color);

        colorIndex = _targetColorIndex;

        targetColor = brickRenderer.material.color;

        transform.GetChild(0).GetComponentInChildren<BoxCollider>().enabled = false;
        }

    }    
}
