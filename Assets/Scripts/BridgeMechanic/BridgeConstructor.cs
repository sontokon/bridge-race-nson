using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeConstructor : MonoBehaviour
{
    [SerializeField] private StackManager stackManager;
    [SerializeField] private PlayerScript playerScript;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("BridgeTile"))
        {
            var stair = other.GetComponent<BridgeTileScript>();
            if (stair.hasBrick == false && stackManager.IsPopAble() == true)
            {
                stackManager.PopBrick();
                stair.ColorBrick(playerScript.playerColorIndex);
                print("colorStair");
            }
            else
            {
                print("no brick can go!");
            }    

        }
    }
}
