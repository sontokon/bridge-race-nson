using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : MonoBehaviour
{
    public NavMeshAgent agent;
    public GameObject testTarget;

    [SerializeField] private List<GameObject> targets = new List<GameObject>();
    [SerializeField] private bool haveTarget = false;
    [SerializeField] private Animator animator;
    
    private Vector3 targetTransform;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;

    }
    private void Update()
    {
        HandleRotation();
        BotState();
    }
    private void HandleRotation()
    {
        float targetAngle = Mathf.Atan2(targetTransform.x, targetTransform.z) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);
    }
    void BotState()
    {
        targetTransform = testTarget.transform.position;
        agent.SetDestination(targetTransform);
        animator.SetInteger("animState", 1);
    }    
   
}
